<form class="form-horizontal">
    <fieldset>
        <!-- Form Name -->
        <legend>Features</legend>

        <!-- Multiple Radios -->
        <div class="form-group">
            <label class="col-md-4 control-label" for="type">Type</label>
            <div class="col-md-4">
                <div class="radio">
                    <label for="type-0">
                        <input type="radio" name="type" id="type-0" value="1" checked="checked">
                        House
                    </label>
                </div>
                <div class="radio">
                    <label for="type-1">
                        <input type="radio" name="type" id="type-1" value="2">
                        Flat
                    </label>
                </div>
            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="rooms">Rooms Number</label>
            <div class="col-md-6">
                <input id="rooms" name="rooms" type="text" placeholder="5" class="form-control input-md" required="">

            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="price">Price</label>
            <div class="col-md-6">
                <input id="price" name="price" type="text" placeholder="$120000" class="form-control input-md" required="">
            </div>
        </div>   
        
        <div class="form-group">
            <label class="col-md-4 control-label" for="price">Tags</label>
            <div class="col-md-6">
                <div id="my-tag-list" class="tag-list"></div>
            </div>
        </div>

    </fieldset>
</form>

<script type="application/javascript">
    $(function() {
        $('#my-tag-list').tags({
            tagData:["furnished", "kitchen","toilet","school","parking","luxury","pool","bathroom","wc","downtown"],
            suggestions:["basic", "suggestions"],
            excludeList:["not", "these", "words"]
        });
    });
</script>
