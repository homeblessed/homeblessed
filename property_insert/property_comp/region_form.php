<form class="form-horizontal" >
    <fieldset>
        <!-- Form Name -->
        <legend>Region</legend>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="firstName">City:</label>
            <div class="col-md-6">
                <input id="city" name="city" type="text" placeholder="Nicosia" class="form-control input-md" required="">

            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="lastName">Address:</label>
            <div class="col-md-6">
                <input id="address" name="address" type="text" placeholder="Gregory Isaac 40" class="form-control input-md" required="">

            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="organization">Post Code:</label>
            <div class="col-md-6">
                <input id="postcode" name="postecode" type="text" placeholder="8723" class="form-control input-md" required="">

            </div>
        </div>
        
        <div class="form-group">
            <label class="col-md-4 control-label" for="organization">Description:</label>
            <div class="col-md-7">
                <textarea id="desc" name="desc" type="text" placeholder="This is a description" class="form-control input-md" required=""></textarea>

            </div>
        </div>
        
    </fieldset>
</form>

