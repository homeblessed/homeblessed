<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>Homeblessed</title>
    <link rel="icon" type="image/png" href="../assets/img/homeblessed_favicon.png">
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../bower_components/sweetalert/dist/sweetalert.css" rel="stylesheet">
    <link href="../bower_components/bootstrap-fileinput/css/fileinput.min.css" rel="stylesheet">
    <link href="../bower_components/bootstrap-tags/dist/css/bootstrap-tags.css" rel="stylesheet">
    <link href="../bower_components/fontawesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="../assets/css/index.css" rel="stylesheet">
    <link href="../assets/css/navbar-index.css" rel="stylesheet">
    <link href="../assets/css/panels.css" rel="stylesheet">

    <script src="../bower_components/jquery/dist/jquery.min.js" ></script>
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../bower_components/bootstrap-fileinput/js/fileinput.min.js"></script>
    <script src="../bower_components/bootstrap-tags/dist/js/bootstrap-tags.min.js"></script>
    <script src="../bower_components/sweetalert/dist/sweetalert.min.js"> </script>
    
    <script type="application/javascript">
        var USER_SETTINGS=null;
        loadSettings();

        function loadSettings(){
            USER_SETTINGS=localStorage.getItem('USER_SETTINGS');
        }

        function saveSettings(settings){
            localStorage.setItem('USER_SETTINGS',settings);
        }
    </script>

</head>
<body>

    <?php include "../general_componets/navbar.php"?>
    <?php include "../modals/sign-in.php"?>

    <div class="container-fluid" style="margin-bottom:  1%;" >
        <div class="panel panel-default" style="margin: 0 10%;">
            <div class="centered panel-heading">
                <h2>Insert New Property</h2>
                <hr class="property-primary">
            </div>

            <br><br>
            <div class="row">
                <div class="col-md-5" style="margin: 0 2%;"> <?php include_once "property_comp/region_form.php" ?> </div>
                <div class="col-md-5" style="margin: 0 2%;"> <?php include_once "property_comp/features.php" ?>      </div>
            </div>



            <br><br>

            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-7">
                 <label class="control-label">Select Images</label>
                 <input id="input-7" multiple type="file" class="file file-loading" data-allowed-file-extensions='["jpg", "png", "gif", "bmp"]'>
                </div>
            </div>

            <br>
            <div class="row">
                <div class="col-md-5"></div>
                <div class="col-md-4">
                    <p><a class="btn btn-primary btn-lg" href="#" role="button"><span class="glyphicon glyphicon-floppy-disk"></span> Submit</a></p>
                </div>
            </div>

        </div>
    </div>

    <script src="../assets/js/navbar.js"> </script>
    <script src="../assets/js/account.js"> </script>
    
</body>
</html>