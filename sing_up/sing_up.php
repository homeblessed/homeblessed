<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>Homeblessed</title>
    <link rel="icon" type="image/png" href="../assets/img/homeblessed_favicon.png">
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../bower_components/sweetalert/dist/sweetalert.css" rel="stylesheet">
    <link href="../bower_components/fontawesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="../assets/css/index.css" rel="stylesheet">
    <link href="../assets/css/panels.css" rel="stylesheet">
    <link href="../assets/css/navbar-index.css" rel="stylesheet">

    <script src="../bower_components/jquery/dist/jquery.min.js" ></script>
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../bower_components/sweetalert/dist/sweetalert.min.js"></script>
    <script type="application/javascript">
        var USER_SETTINGS=null;
        loadSettings();

        function loadSettings(){
            USER_SETTINGS=localStorage.getItem('USER_SETTINGS');
        }

        function saveSettings(settings){
            localStorage.setItem('USER_SETTINGS',settings);
        }
    </script>

</head>
<body>

    <?php include "../general_componets/navbar.php"?>


    <div class="container-fluid" >
    <div class="panel panel-default" style="margin: 0 15%;">

        <div class=" centered panel-heading">
            <h2> Register</h2>
             <hr class="details-primary">
        </div>
        <br><br>
        <div class="row">
            <div class="col-md-5" style="margin:0 2%"> <?php include_once "sing_up_components/agent-details_form.php" ?> </div>
            <div class="col-md-6"> <?php include_once "sing_up_components/location_form.php" ?>      </div>
        </div>

        <br> <br>

        <div class="row">
            <div class="col-md-5" style="margin:0 2%"> <?php include_once "sing_up_components/contact-details-form.php" ?></div>
            <div class="col-md-6"> <?php include_once "sing_up_components/validation-form.php" ?></div>
        </div>

        <br><br>

        <div class="row">
            <div class="col-md-5"></div>
            <div class="col-md-4">
            <p><a class="btn btn-primary btn-lg" href="#" role="button">Register</a></p>
            </div>
        </div>


    </div>
    </div>    <!-- Container Fluid End-->

        <script src="../assets/js/account.js"></script>
        <script src="../assets/js/navbar.js"></script>
    

        <?php include('../modals/sign-in.php'); ?>    
    
    <script type="application/javascript">
        $('#profile-toggle').addClass('hidden');
        $('#sign-out-button').addClass('hidden');
        $('#sign-in-button').removeClass('hidden');
        $('#register-button').removeClass('hidden');
    </script>
        
</body>
</html>