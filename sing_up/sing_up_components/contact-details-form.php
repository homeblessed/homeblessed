<form class="form-horizontal">
    <fieldset>

        <!-- Form Name -->
        <legend>Contact Details </legend>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="email">Email</label>
            <div class="col-md-6">
                <input id="email" name="email" type="text" placeholder="example@gmail.com" class="form-control input-md" required="">

            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="mobileNumber">Mobile Number</label>
            <div class="col-md-6">
                <input id="mobileNumber" name="mobileNumber" type="text" placeholder="991234567" class="form-control input-md">

            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="officeNumber">Office Number</label>
            <div class="col-md-6">
                <input id="officeNumber" name="officeNumber" type="text" placeholder="22123456" class="form-control input-md">

            </div>
        </div>

    </fieldset>
</form>