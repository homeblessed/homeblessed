<form class="form-horizontal">
    <fieldset>

        <!-- Form Name -->
        <legend>Almost Done!</legend>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="aboutUs">How do you here about us?</label>
            <div class="col-md-6">
                <input id="aboutUs" name="aboutUs" type="text" placeholder="Google, Yahoo" class="form-control input-md" required="">

            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="botsValidation">Validation Of Bots:</label>
            <div class="col-md-6">
                <input id="botsValidation" name="botsValidation" type="text" placeholder="" class="form-control input-md">

            </div>
        </div>

    </fieldset>
</form>