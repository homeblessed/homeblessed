<form class="form-horizontal">
    <fieldset>

        <!-- Form Name -->
        <legend>Office Location </legend>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="address">Address</label>
            <div class="col-md-6">
                <input id="address" name="address" type="text" placeholder="Agiou Dometiou 77" class="form-control input-md">

            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="city">City</label>
            <div class="col-md-6">
                <input id="city" name="city" type="text" placeholder="Nicosia" class="form-control input-md">

            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="postcode">Post Code</label>
            <div class="col-md-6">
                <input id="postcode" name="postcode" type="text" placeholder="8540" class="form-control input-md">

            </div>
        </div>

    </fieldset>
</form>