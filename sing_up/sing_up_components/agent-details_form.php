<form class="form-horizontal" >
    <fieldset>
        <!-- Form Name -->
        <legend>Agent Details</legend>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="firstName">First Name</label>
            <div class="col-md-6">
                <input id="firstName" name="firstName" type="text" placeholder="Theodoros" class="form-control input-md" required="">

            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="lastName">Last Name</label>
            <div class="col-md-6">
                <input id="lastName" name="lastName" type="text" placeholder="Van Persie" class="form-control input-md" required="">

            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="organization">Organization</label>
            <div class="col-md-6">
                <input id="organization" name="organization" type="text" placeholder="Constructor LTD" class="form-control input-md">

            </div>
        </div>

    </fieldset>
</form>