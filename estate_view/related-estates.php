<div class="container">
    <div class="future">
            <div class="centered">
                <h2>Related Real Estates</h2>
                <hr class="property-related">
            </div>
        </div>
        <div class="content-bottom-in">
            <ul id="flexiselDemo1">
                <li>
                    <div class="project-fur">
                        <a href="estate-view.php"><img class="img-responsive" src="assets/img/pi.jpg" alt=""/> </a>

                        <div class="fur">
                            <div class="fur1">
                                <span class="fur-money">$2.44 Lacs - 5.28 Lacs </span>
                                <h6 class="fur-name"><a href="estate-view.php">Contrary to popular</a></h6>
                                <span>Paris</span>
                            </div>
                            <div class="fur2">
                                <span>2 BHK</span>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="project-fur">
                        <a href="estate-view.php"><img class="img-responsive" src="assets/img/pi1.jpg" alt=""/> </a>

                        <div class="fur">
                            <div class="fur1">
                                <span class="fur-money">$2.44 Lacs - 5.28 Lacs </span>
                                <h6 class="fur-name"><a href="estate-view.php">Contrary to popular</a></h6>
                                <span>Paris</span>
                            </div>
                            <div class="fur2">
                                <span>2 BHK</span>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="project-fur">
                        <a href="estate-view.php"><img class="img-responsive" src="assets/img/pi2.jpg" alt=""/> </a>

                        <div class="fur">
                            <div class="fur1">
                                <span class="fur-money">$2.44 Lacs - 5.28 Lacs </span>
                                <h6 class="fur-name"><a href="estate-view.php">Contrary to popular</a></h6>
                                <span>Paris</span>
                            </div>
                            <div class="fur2">
                                <span>2 BHK</span>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="project-fur">
                        <a href="estate-view.php"><img class="img-responsive" src="assets/img/pi3.jpg" alt=""/> </a>

                        <div class="fur">
                            <div class="fur1">
                                <span class="fur-money">$2.44 Lacs - 5.28 Lacs </span>
                                <h6 class="fur-name"><a href="estate-view.php">Contrary to popular</a></h6>
                                <span>Paris</span>
                            </div>
                            <div class="fur2">
                                <span>2 BHK</span>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>