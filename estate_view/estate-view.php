<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="">

    <title>Homeblessed</title>

    <!-- CSS -->
    <link rel="icon" type="image/png" href="../assets/img/homeblessed_favicon.png">
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../bower_components/ideal-image-slider/ideal-image-slider.css" rel="stylesheet">
    <link href="../bower_components/fontawesome/css/font-awesome.min.css"
    <link href="assets/css/estate-view.css" rel="stylesheet">
    <link href="../assets/css/index.css" rel="stylesheet">
    <link rel="stylesheet" href="../bower_components/flexslider/flexslider.css" type="text/css" media="screen"/>
    <link href="assets/css/estate-view.css" rel="stylesheet">
    <link href="../assets/css/index.css" rel="stylesheet">
    <link href="../assets/css/navbar-index.css" rel="stylesheet">
    <link href="../assets/css/panels.css" rel="stylesheet">
    <!-- scripts -->

    <script src="../bower_components/jquery/dist/jquery.min.js"></script>
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../bower_components/ideal-image-slider/ideal-image-slider.js"></script>
    <script src="assets/js/jssor.slider.min.js"></script>
    <script defer src="../bower_components/flexslider/jquery.flexslider-min.js"></script>
    <script type="text/javascript" src="../bower_components/flexisel/js/jquery.flexisel.js"></script>

    <!-- Custom styles for this template -->


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>-->
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <script type="application/javascript">
            var USER_SETTINGS=null;
            loadSettings();
            
            function loadSettings(){
                USER_SETTINGS=localStorage.getItem('USER_SETTINGS');
            }

            function saveSettings(settings){
                localStorage.setItem('USER_SETTINGS',settings);
            }
        </script>
</head>
<body>
<?php include '../general_componets/navbar.php'; ?>
<div class="row">
    <div class="col-lg-push-1 col-lg-6">
        <br>
        <!--        --><?php //include 'related-estates.php'; ?>


            <div class=" buying-top">
                <div class="flexslider">
                    <ul class="slides">
                        <li data-thumb="assets/img/ss.jpg">
                            <img src="assets/img/ss.jpg"/>
                        </li>
                        <li data-thumb="assets/img/ss1.jpg">
                            <img src="assets/img/ss1.jpg"/>
                        </li>
                        <li data-thumb="assets/img/ss2.jpg">
                            <img src="assets/img/ss2.jpg"/>
                        </li>
                        <li data-thumb="assets/img/ss3.jpg">
                            <img src="assets/img/ss3.jpg"/>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- FlexSlider -->

            <div>
                <div class="buy-sin-single">
                    <div class="col-sm-5 immediate">
                        <h2 class="top-bot-margin street-address">Kurillou,13</h2>
                        <h2 class="top-bot-margin street-address">Strovolos,Nicosia 2028</h2>
                        <h3 class="top-bot-margin">€500,000</h3>
                        <h3 class="top-bot-margin">Views: <span> 120</span></h3>

                        <h4 class="features">Features</h4>

                        <p><span class="bath">Bed </span>: <span class="middle-side two">2 BHK</span></p>

                        <p><span class="bath1">Baths </span>: <span class="middle-side two">2</span></p>

                        <p><span class="bath2">Built-up Area</span>: <span
                                class="middle-side two">100 Sq.Yrds</span></p>

                        <p><span class="bath3">Plot Area </span>:<span class="middle-side two"> 150 Sq.Yrds</span>
                        </p>

                        <p><span class="bath4">Age of property</span> : <span
                                class="middle-side two">4 - 10 Years</span></p>

                        <p><span class="bath5">Price </span>:<span class="middle-side two"> 30-40 Lacs</span></p>

                    </div>
                    <div class="col-sm-7 buy-sin">

                        <h2 class="real-estate-description">Description</h2>

                        <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a
                            piece of
                            classical Latin literature from 45 BC, making it over 2000 years old. Richard
                            McClintock, a
                            Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure
                            Latin
                            words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word
                            in
                            classical literature</p>

                        <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a
                            piece of
                            classical Latin literature from 45 BC </p>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-5 col-lg-push-2">
            <br>
            <h2>Request Information</h2>
            <br>
            <div class="seller-info row">
                <div class="col-lg-5 col-md-5 col-xs-12">
                    <img src="../assets/img/RickGrimes.jpg" style="width:95px;height: 80px">
                </div>
                <div class="col-lg-pull-3 col-lg-5 col-md-5 col-xs-12">
                    <h3>Rick Grimes</h3>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star-half-o"></i>
                    <h4>+357-43215678</h4>
                </div>
            </div>
            <br>
            <form>
                <div class="form-group row">
                    <div class="col-lg-5 col-md-5">
                        <input type="text" class="form-control" id="req-info-name" required=""
                               placeholder="Full Name">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-5 col-md-5">
                        <input type="email" class="form-control" id="req-info-email" required=""
                               placeholder="Email">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-5 col-md-5">
                        <input type="text" class="form-control" id="req-info-phone" required="" placeholder="Phone">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-5 col-md-5">
                        <textarea class="form-control" id="req-info-comments" name="textarea">I'd like more information about this house</textarea>
                    </div>
                </div>
                <button class="btn btn-primary">Find out more</button>
            </form>
        </div>
    </div>
    <div class="map-buy-single container">
        <h4>Neighborhood Info</h4>
        <div class="map-buy-single1">
            <iframe src="https://www.google.com/maps/embed/v1/place?q=Nicosia,Strovolos&key=AIzaSyAN0om9mFmy1QN6Wf54tXAowK4eT0ZUPrU"></iframe>

        </div>
    </div>
    <?php include 'related-estates.php';?>
    <script src="assets/js/estate-view.js"></script>
    <script src="../assets/js/navbar.js"></script>
    <script src="../assets/js/account.js"></script>
</body>
</html>