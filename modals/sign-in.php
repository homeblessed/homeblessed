
<div id="sign-in-modal" class="modal fade" role="dialog" aria-labelledby="gridSystemModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title" id="gridSystemModalLabel">Sign-in</h3>
            </div>
            <div class="modal-body">
                <form id="sign-in-form">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email address</label>
                        <input type="email" class="form-control" id="exampleInputEmail1" required="" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Password</label>
                        <input type="password" class="form-control" required="" id="exampleInputPassword1" placeholder="Password">
                    </div>
                    <button type="submit" class="btn btn-primary">Sign in</button>

                </form>
            </div>
        </div>
    </div>
</div>

<script type="application/javascript">
    $('#sign-in-form').submit(function(e){
        var settings = { 'email':$('#exampleInputEmail1').val(), 'password':$('#exampleInputPassword1').val() };
        e.preventDefault();
        
        console.log(settings);
        saveSettings(settings);
        
        $('#profile-toggle').removeClass('hidden');
        $('#sign-in-button').addClass('hidden');
        $('#register-button').addClass('hidden');
        $('#sign-out-button').removeClass('hidden');
        
        $('#sign-in-modal').modal('toggle');
    });
</script>