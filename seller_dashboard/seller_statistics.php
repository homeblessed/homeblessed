<h1 class="page-header">Statistics</h1>
<div class="row">
    <div class="col-lg-6">
        <div class="panel panel-info">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-lg-6">
                        <h4>Real estates sold</h4>
                    </div>
                    <div class="col-lg-3 col-lg-push-2 form-group">
                        <select name="select" class="form-control" style="height:37px">
                            <option value="value1">2012</option>
                            <option value="value2">2013</option>
                            <option value="value3">2014</option>
                            <option value="value4" selected>2015</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div id="real-estates-sold" style="height:300px">
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="panel panel-info">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-lg-6">
                        <h4>Real estates rented</h4>
                    </div>
                    <div class="col-lg-3 col-lg-push-2 form-group">
                        <select name="select" class="form-control" style="height:37px">
                            <option value="value1">2012</option>
                            <option value="value2">2013</option>
                            <option value="value3">2014</option>
                            <option value="value4" selected>2015</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div id="real-estates-rented" style="height:300px">
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-6">
        <div class="panel panel-info">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-lg-7">
                        <h4>House vs Apartments sold</h4>
                    </div>
                    <div class="col-lg-3 col-lg-push-2 form-group">
                        <select name="select" class="form-control" style="height:37px">
                            <option value="value1">2012</option>
                            <option value="value2">2013</option>
                            <option value="value3">2014</option>
                            <option value="value4" selected>2015</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div id="house-apartments-sold" style="height:300px">
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="panel panel-info">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-lg-7">
                        <h4>House vs Apartments rented</h4>
                    </div>
                    <div class="col-lg-3 col-lg-push-2 form-group">
                        <select name="select" class="form-control" style="height:37px">
                            <option value="value1">2012</option>
                            <option value="value2">2013</option>
                            <option value="value3">2014</option>
                            <option value="value4" selected>2015</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div id="house-apartments-rented" style="height:300px">
                </div>
            </div>
        </div>
    </div>
