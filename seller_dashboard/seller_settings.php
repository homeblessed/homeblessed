<h1 class="page-header">Profile settings</h1>
<p>Fields marked with an * are compulsory and must be filled. Press the save changes button once you are happy to
    proceed.</p>
<div class="panel panel-default">
    <div class="centered panel-heading">
        <h2>Your details</h2>
        <hr class="details-primary">
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-6">
                <form>
                    <div class="form-group">
                        <label for="exampleInputEmail1">First Name*</label>

                        <div class="row">
                            <div class="col-lg-10">
                                <input type="text" class="form-control" required="" id="exampleInputEmail1"
                                       value="Rick">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Last Name*</label>

                        <div class="row">
                            <div class="col-lg-10">
                                <input type="text" class="form-control" required="" id="exampleInputEmail1"
                                       value="Grimes">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Real Estate Agency*</label>

                        <div class="row">
                            <div class="col-lg-10">
                                <input type="text" required="" class="form-control" id="exampleInputEmail1"
                                       value="Terminus Homes">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Mobile Number*</label>

                        <div class="row">
                            <div class="col-lg-10">
                                <input type="tel" value="930192309" required="" class="form-control" id="exampleInputEmail1">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Office Number*</label>

                        <div class="row">
                            <div class="col-lg-10">
                                <input type="tel" class="form-control" required="" id="exampleInputEmail1" value="321231231">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-lg-6">
                <form>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Adress*</label>

                        <div class="row">
                            <div class="col-lg-10">
                                <input type="text" class="form-control" required="" id="Walking Dead Street,13" value="Walking Dead Street,13">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">City*</label>

                        <div class="row">
                            <div class="col-lg-10">
                                <input type="text" class="form-control" required="" id="ZombieCIty"
                                       value="ZombieCity">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Post Code*</label>

                        <div class="row">
                            <div class="col-lg-10">
                                <input type="text" class="form-control" required="" id="exampleInputEmail1"
                                       value="0000">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email*</label>

                        <div class="row">
                            <div class="col-lg-10">
                                <input type="email" class="form-control" required="" id="exampleInputEmail1" value="rick_grimes@amc.com">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Password*</label>

                        <div class="row">
                            <div class="col-lg-10">
                                <input type="password" class="form-control" required="" id="exampleInputEmail1" value="zombiecarnage">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-lg-push-6">
                    <button type="submit" class="btn btn-primary">Save Changes</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
