//Loads the correct sidebar on window load,
//collapses the sidebar on window resize.
// Sets the min-height of #page-wrapper to window size
$(function() {
    $(window).bind("load resize", function() {
        topOffset = 50;
        width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
        if (width < 768) {
            $('div.navbar-collapse').addClass('collapse');
            topOffset = 100; // 2-row-menu
        } else {
            $('div.navbar-collapse').removeClass('collapse');
        }

        height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
        height = height - topOffset;
        if (height < 1) height = 1;
        if (height > topOffset) {
            $("#page-wrapper").css("min-height", (height) + "px");
        }
    });
    $('#side-menu a').click(function(){
        var pageName = $(this).attr('id');
        $('#side-menu a').each(function(){
            if($(this).hasClass('active')){
                $(this).removeClass('active');
            }
        });
        $(this).addClass('active');
        $.get( ""+pageName+".php", function( data ) {
            $('#page-wrapper .container-fluid').empty();
            $('#page-wrapper .container-fluid').append(data);
            if(pageName=='seller_statistics'){
                loadCharts();
            }
            if(pageName=='seller_listings'){
                $('#page-selection').bootpag({
                    total: 3,
                    page:1,
                    maxVisible:2,
                    next: 'Next',
                    prev: 'Previous'
                }).on("page", function (event, num) {
                    $(".page-feedback").remove();
                    $(".page-header").after('<h5 style="text-align:right" class="page-feedback">Page ' + num + ' of 18 results</h5>');
                    $("html, body").animate({scrollTop: 0}, "slow");

                });
            }
        });
    });
    function loadCharts() {
        var randomScalingFactor = function () {
            return Math.round(Math.random() * 100)
        };
        var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun'];

        new Morris.Line({
            // ID of the element in which to draw the chart.
            element: 'real-estates-sold',
            // Chart data records -- each entry in this array corresponds to a point on
            // the chart.
            data: [
                {month: '2015-01', value: randomScalingFactor()},
                {month: '2015-02', value: randomScalingFactor()},
                {month: '2015-03', value: randomScalingFactor()},
                {month: '2015-04', value: randomScalingFactor()},
                {month: '2015-05', value: randomScalingFactor()},
                {month: '2015-06', value: randomScalingFactor()}
            ],
            // The name of the data record attribute that contains x-values.
            xkey: 'month',
            xLabelFormat: function (x) { return months[x.getMonth()];},
            // A list of names of data record attributes that contain y-values.
            ykeys: ['value'],
            // Labels for the ykeys -- will be displayed when you hover over the
            // chart.
            labels: ['Number of real estates'],
            resize:true
        });
        new Morris.Line({
            // ID of the element in which to draw the chart.
            element: 'real-estates-rented',
            // Chart data records -- each entry in this array corresponds to a point on
            // the chart.
            data: [
                {month: '2015-01', value: randomScalingFactor()},
                {month: '2015-02', value: randomScalingFactor()},
                {month: '2015-03', value: randomScalingFactor()},
                {month: '2015-04', value: randomScalingFactor()},
                {month: '2015-05', value: randomScalingFactor()},
                {month: '2015-06', value: randomScalingFactor()}
            ],
            // The name of the data record attribute that contains x-values.
            xkey: 'month',
            xLabelFormat: function (x) { return months[x.getMonth()];},
            // A list of names of data record attributes that contain y-values.
            ykeys: ['value'],
            // Labels for the ykeys -- will be displayed when you hover over the
            // chart.
            labels: ['Number of real estates'],
            resize:true
        });
        Morris.Donut({
            element: 'house-apartments-sold',
            data: [
                {label: "Apartment Sales", value: randomScalingFactor()},
                {label: "House Sales", value: randomScalingFactor()},
            ]
        });
        Morris.Donut({
            element: 'house-apartments-rented',
            data: [
                {label: "Apartment Rents", value: randomScalingFactor()},
                {label: "House Rents", value: randomScalingFactor()},
            ]
        });
    }

});
