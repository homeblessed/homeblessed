<h1 class="page-header">Your Listings</h1>
<h5 style="text-align:right" class="page-feedback">Page 1 of 18 results</h5>
<div id="products" class="row list-group">
    <div class="item col-md-4">
        <div class="thumbnail">
            <img class="group list-group-image" src="../assets/img/listings/1.jpg" alt="" />
            <div class="caption">
                <h4 class="group inner list-group-item-heading">
                    House<span class="text-muted"> Park Ave Shawnee 74804 </span></h4>
                <p class="group inner list-group-item-text">
                    Only minutes separate you from banking, shopping, colleges, medical facilities and much more in this cute home with a great oversized back yard! </p>
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <p class="lead">
                            $55.000</p>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <a class="btn btn-primary" href="../estate_view/estate-view.php">View details</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="item  col-lg-4">
        <div class="thumbnail">
            <img class="group list-group-image" src="../assets/img/listings/2.jpg" alt="" />
            <div class="caption">
                <h4 class="group inner list-group-item-heading">
                    House<span class="text-muted"> Park Ave Shawnee 74804 </span></h4>
                <p class="group inner list-group-item-text">
                    Only minutes separate you from banking, shopping, colleges, medical facilities and much more in this cute home with a great oversized back yard! </p>
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <p class="lead">
                            $55.000</p>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <a class="btn btn-primary" href="../estate_view/estate-view.php">View details</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="item  col-lg-4">
        <div class="thumbnail">
            <img class="group list-group-image" src="../assets/img/listings/5.jpg" alt="" />
            <div class="caption">
                <h4 class="group inner list-group-item-heading">
                    Apartment<span class="text-muted"> Park Ave Shawnee 74803 </span></h4>
                <p class="group inner list-group-item-text">
                    Only minutes separate you from banking, shopping, colleges, medical facilities and much more in this cute home with a great oversized back yard! </p>
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <p class="lead">
                            $450/month</p>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <a class="btn btn-primary" href="../estate_view/estate-view.php">View details</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="item col-lg-4">
        <div class="thumbnail">
            <img class="group list-group-image" src="../assets/img/listings/3.jpg" alt="" />
            <div class="caption">
                <h4 class="group inner list-group-item-heading">
                    House<span class="text-muted"> Park Ave Shawnee 74804 </span></h4>
                <p class="group inner list-group-item-text">
                    Only minutes separate you from banking, shopping, colleges, medical facilities and much more in this cute home with a great oversized back yard! </p>
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <p class="lead">
                            $55.000</p>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <a class="btn btn-primary" href="../estate_view/estate-view.php">View details</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="item col-lg-4">
        <div class="thumbnail">
            <img class="group list-group-image" src="../assets/img/listings/4.jpg" alt="" />
            <div class="caption">
                <h4 class="group inner list-group-item-heading">
                    Apartment<span class="text-muted"> Park Ave Shawnee 74804 </span></h4>
                <p class="group inner list-group-item-text">
                    Only minutes separate you from banking, shopping, colleges, medical facilities and much more in this cute home with a great oversized back yard! </p>
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <p class="lead">
                            $550/month</p>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <a class="btn btn-primary" href="../estate_view/estate-view.php">View details</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="item col-lg-4">
        <div class="thumbnail">
            <img class="group list-group-image" src="../assets/img/listings/6.jpg" alt="" />
            <div class="caption">
                <h4 class="group inner list-group-item-heading">
                    Apartment<span class="text-muted"> Park Ave Shawnee 74804 </span></h4>
                <p class="group inner list-group-item-text">
                    Only minutes separate you from banking, shopping, colleges, medical facilities and much more in this cute home with a great oversized back yard! </p>
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <p class="lead">
                            $250/month</p>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <a class="btn btn-primary" href="../estate_view/estate-view.php">View details</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<p style="text-align: center" id="page-selection"></p>
