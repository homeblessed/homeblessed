<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Homeblessed</title>
    <link rel="icon" type="image/png" href="../assets/img/homeblessed_favicon.png">
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../bower_components/fontawesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="../bower_components/sweetalert/dist/sweetalert.css" rel="stylesheet">
    <link href="../bower_components/morris.js/morris.css" rel="stylesheet">
    <link href="assets/css/seller-dashboard.css" rel="stylesheet">
    <link href="../assets/css/navbar-index.css" rel="stylesheet">
    <link href="../assets/css/index.css" rel="stylesheet">
    <link href="../assets/css/panels.css" rel="stylesheet">
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>
    <script src="../bower_components/jquery-bootpag/lib/jquery.bootpag.min.js"></script>
    <script type="application/javascript">
        var USER_SETTINGS=null;
        loadSettings();

        function loadSettings(){
            USER_SETTINGS=localStorage.getItem('USER_SETTINGS');
        }

        function saveSettings(settings){
            localStorage.setItem('USER_SETTINGS',settings);
        }
    </script>
</head>
<body>
    <?php include '../general_componets/navbar.php'?>
    <?php include '../modals/sign-in.php'?>
    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
                <li>
                    <a href="#" id="seller_settings"><i class="fa fa-user fa-fw"></i>Profile settings</a>
                </li>
                <li>
                    <a href="#" id="seller_listings"><i class="fa fa-list fa-fw"></i>Your real estates</a>
                </li>
                <li>
                    <a href="#" id="seller_statistics"><i class="fa fa-line-chart fa-fw"></i>Statistics</a>
                </li>
            </ul>
        </div>
        <!-- /.sidebar-collapse -->
    </div>
    <!-- /.navbar-static-side -->
    </nav>

    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
                    <?php include 'seller_settings.php'; ?>
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->
    <!-- Custom Theme JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../bower_components/raphael/raphael-min.js"></script>
    <script src="../bower_components/morris.js/morris.min.js"></script>

<!--    <script src="../bower_components/Chart.js/Chart.min.js"></script>-->
    <script src="assets/js/seller-dashboard.js"></script>
        <script src="../assets/js/navbar.js"></script>
        <script src="../assets/js/account.js"></script>
    </div>
</body>
