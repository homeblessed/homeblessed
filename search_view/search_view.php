<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Homeblessed</title>
        <link rel="icon" type="image/png" href="../assets/img/homeblessed_favicon.png">
        <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="../bower_components/fontawesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="../bower_components/sweetalert/dist/sweetalert.css" rel="stylesheet">
        <link href="../bower_components/bootstrap-slider/slider.css" rel="stylesheet">
        <link href="../bower_components/bootstrap-tags/dist/css/bootstrap-tags.css" rel="stylesheet">
        <link href="../assets/css/listings.css" rel="stylesheet">
        <link href="../assets/css/search-view.css" rel="stylesheet">
        <link href="../assets/css/navbar-index.css" rel="stylesheet">
                <script src="../bower_components/jquery/dist/jquery.min.js"></script>
        <script src="../bower_components/jquery-bootpag/lib/jquery.bootpag.min.js"></script>
        <script type="application/javascript">
            var USER_SETTINGS=null;
            loadSettings();
            
            function loadSettings(){
                USER_SETTINGS=localStorage.getItem('USER_SETTINGS');
            }

            function saveSettings(settings){
                localStorage.setItem('USER_SETTINGS',settings);
            }
        </script>
        
    </head>
    <body>
        <?php include '../general_componets/navbar.php'?>
        <?php include "../modals/sign-in.php"?>                
        <div class="container-fluid">
            <div class="row">
                <div class="side-menu col-md-3">
                    <ul class="list-group">
                        
                        <li class="first-item list-group-item">
                            <h4 class="centered">Search</h4>
                        </li>
                        
                        <li class="list-group-item">
                            <p>Sale <input name="rent-or-sale" id="rent-or-sale-0" value="1" type="checkbox"></p>
                            <p>Rent <input name="rent-or-sale" id="rent-or-sale-1" value="2" type="checkbox"></p>
                        </li>
                        
                        <li class="list-group-item">
                            <b><span id="left-value">$1000 </span></b>
                            <input id="price-range-input" type="text" class="span2" value="" data-slider-min="0" data-slider-max="10000" data-slider-step="5" data-slider-value="[1000,9000]"/>
                            <b><span id="right-value"> $9000</span></b>
                        </li>
                        
                        <li class="list-group-item">
                            <p>House <input name="type" id="type-0" value="1" type="checkbox"></p>
                            <p>Appartment <input name="type" id="type-1" value="2" type="checkbox"></p>
                        </li>
                        
                        <li class="list-group-item">
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1">
                                    <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span></span>
                                <input type="text" class="form-control" placeholder="Location" aria-describedby="basic-addon1">
                            </div>
                        </li>
                        
                        <li class="list-group-item">
                            <div id="my-tag-list" class="tag-list"></div>
                        </li>
                        
                        <li class="centered last-item list-group-item">
                             <a class="btn btn-primary" href="#"><span class="glyphicon glyphicon-search" aria-hidden="true"></span>Search</a>
                        </li>
                        
                    </ul>
                </div>
                
                <div class="col-md-9 listings">
                    <section id="new-listings">
                        <h5 style="text-align:right" class="page-feedback">Page 1 of 60 results</h5>
                        <div id="products" class="row list-group">
                            <div class="item  col-xs-4 col-lg-4">
                                <div class="thumbnail">
                                    <img class="group list-group-image" src="../assets/img/listings/1.jpg" alt="" />
                                    <div class="caption">
                                        <h4 class="group inner list-group-item-heading">
                                            House<span class="text-muted"> Park Ave Shawnee 74804 </span></h4>
                                        <p class="group inner list-group-item-text">
                                           Only minutes separate you from banking, shopping, colleges, medical facilities and much more in this cute home with a great oversized back yard! </p>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-6">
                                                <p class="lead">
                                                    $55.000</p>
                                            </div>
                                            <div class="col-xs-12 col-md-6">
                                                <a class="btn btn-success" href="../estate_view/estate-view.php">View details</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item  col-xs-4 col-lg-4">
                                <div class="thumbnail">
                                    <img class="group list-group-image" src="../assets/img/listings/2.jpg" alt="" />
                                    <div class="caption">
                                        <h4 class="group inner list-group-item-heading">
                                            House<span class="text-muted"> Park Ave Shawnee 74804 </span></h4>
                                        <p class="group inner list-group-item-text">
                                           Only minutes separate you from banking, shopping, colleges, medical facilities and much more in this cute home with a great oversized back yard! </p>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-6">
                                                <p class="lead">
                                                    $55.000</p>
                                            </div>
                                            <div class="col-xs-12 col-md-6">
                                                <a class="btn btn-success" href="../estate_view/estate-view.php">View details</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item  col-xs-4 col-lg-4">
                                <div class="thumbnail">
                                    <img class="group list-group-image" src="../assets/img/listings/5.jpg" alt="" />
                                    <div class="caption">
                                        <h4 class="group inner list-group-item-heading">
                                            Apartment<span class="text-muted"> Park Ave Shawnee 74803 </span></h4>
                                        <p class="group inner list-group-item-text">
                                           Only minutes separate you from banking, shopping, colleges, medical facilities and much more in this cute home with a great oversized back yard! </p>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-6">
                                                <p class="lead">
                                                    $450/month</p>
                                            </div>
                                            <div class="col-xs-12 col-md-6">
                                                <a class="btn btn-success" href="#">View details</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item  col-xs-4 col-lg-4">
                                <div class="thumbnail">
                                    <img class="group list-group-image" src="../assets/img/listings/3.jpg" alt="" />
                                    <div class="caption">
                                        <h4 class="group inner list-group-item-heading">
                                            House<span class="text-muted"> Park Ave Shawnee 74804 </span></h4>
                                        <p class="group inner list-group-item-text">
                                           Only minutes separate you from banking, shopping, colleges, medical facilities and much more in this cute home with a great oversized back yard! </p>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-6">
                                                <p class="lead">
                                                    $55.000</p>
                                            </div>
                                            <div class="col-xs-12 col-md-6">
                                                <a class="btn btn-success" href="../estate_view/estate-view.php">View details</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item  col-xs-4 col-lg-4">
                                <div class="thumbnail">
                                    <img class="group list-group-image" src="../assets/img/listings/4.jpg" alt="" />
                                    <div class="caption">
                                        <h4 class="group inner list-group-item-heading">
                                            Apartment<span class="text-muted"> Park Ave Shawnee 74804 </span></h4>
                                        <p class="group inner list-group-item-text">
                                           Only minutes separate you from banking, shopping, colleges, medical facilities and much more in this cute home with a great oversized back yard! </p>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-6">
                                                <p class="lead">
                                                    $550/month</p>
                                            </div>
                                            <div class="col-xs-12 col-md-6">
                                                <a class="btn btn-success" href="../estate_view/estate-view.php">View details</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item  col-xs-4 col-lg-4">
                                <div class="thumbnail">
                                    <img class="group list-group-image" src="../assets/img/listings/6.jpg" alt="" />
                                    <div class="caption">
                                        <h4 class="group inner list-group-item-heading">
                                            Apartment<span class="text-muted"> Park Ave Shawnee 74804 </span></h4>
                                        <p class="group inner list-group-item-text">
                                           Only minutes separate you from banking, shopping, colleges, medical facilities and much more in this cute home with a great oversized back yard! </p>
                                        <div class="row">
                                            <div class="col-xs-12 col-md-6">
                                                <p class="lead">
                                                    $250/month</p>
                                            </div>
                                            <div class="col-xs-12 col-md-6">
                                                <a class="btn btn-success" href="../estate_view/estate-view.php">View details</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <p style="text-align: center" id="page-selection"></p>
                </div>
            </div>
        </div>
        
        <footer class="footer">
            <div class="container">
                <span>Copyright © Homeblessed 2015 </span>
                <ul class="social-networks">
                    <li><a target="_blank" href="#"><i class="fa fa-facebook-square"></i></a></li>
                    <li><a target="_blank" href="#"><i class="fa fa-twitter-square"></i></a></li>
                    <li><a target="_blank" href="#"><i class="fa fa-instagram"></i></a></li>
                </ul>
            </div>
        </footer>
        
        <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="../bower_components/sweetalert/dist/sweetalert.min.js"></script>
        <script src="../bower_components/bootstrap-slider/bootstrap-slider.js"></script>
        <script src="../bower_components/bootstrap-tags/dist/js/bootstrap-tags.min.js"></script>
        <script src="../assets/js/search-view.js"></script>
        <script src="../assets/js/account.js"></script>
        <script src="../assets/js/navbar.js"></script>
    </body>
</html>