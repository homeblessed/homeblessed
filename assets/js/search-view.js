$("#price-range-input").slider({});
$("#price-range-input").on('slideStop', changeValues);

function changeValues(e) {
    var range = $("#price-range-input").data('slider').getValue();
    $('#left-value').html('$' + range[0]);
    $('#right-value').html('$' + range[1]);
}

$(function () {
    $('#my-tag-list').tags({
        tagData: ["furnished", "kitchen", "toilet", "school", "parking", "luxury", "pool", "bathroom", "wc", "downtown"],
        suggestions: ["basic", "suggestions"],
        excludeList: ["not", "these", "words"]
    });
});


$('#page-selection').bootpag({
    total: 10,
    page:1,
    maxVisible:5,
    next: 'Next',
    prev: 'Previous'
}).on("page", function (event, num) {
    $(".page-feedback").remove();
    $("#new-listings").prepend('<h5 style="text-align:right" class="page-feedback">Page ' + num + ' of 60 results</h5>');
    $("html, body").animate({scrollTop: 0}, "slow");

});

$( "#page-selection .pagination li[data-lp='1']:not(.prev)" ).addClass('active');
