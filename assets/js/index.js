$(document).ready(function() {
    $('#list').click(function(event){event.preventDefault();$('#products .item').addClass('list-group-item');});
    $('#grid').click(function(event){event.preventDefault();$('#products .item').removeClass('list-group-item');$('#products .item').addClass('grid-group-item');});
});

$('body').scrollspy({
    target: '.navbar-fixed-top',
});

$(function() {
    $('.page-scroll a').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
    });
});

$('.navbar-collapse ul li a').click(function() {
    $('.navbar-toggle:visible').click();
});

$('.nav-main-options li').click(function(){
    $('.nav-main-options li').removeClass('active');
    $(this).addClass('active');
});

$('#buy-btn').click(function(e){
    $('#buy-btn').addClass('btn-selected');
    $('#rent-btn').removeClass('btn-selected');
});

$('#rent-btn').click(function(e){
    $('#buy-btn').removeClass('btn-selected');
    $('#rent-btn').addClass('btn-selected');
});