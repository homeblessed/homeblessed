<nav class="navbar navbar-default">
    <div class="container-fluid">
        
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="http://localhost/homeblessed/"><img id="homeblessed-logo-brand" src="../assets/img/homeblessed_favicon.png"/></a>
        </div>

        <div class="collapse navbar-collapse">
            <ul class="nav-main-options nav navbar-nav">
                <li class="active"><a href="..#search-section-anchor">Home <span class="sr-only">(current)</span></a></li>
                <li><a href="..#why-homeblessed-anchor">Why us</a></li>
                <li><a href="..#new-listings-anchor">What's new</a></li>
                <li><a href="..#contact-us-anchor"> Contact us </a></li>
            </ul>
            <form class="navbar-form navbar-left" id="search-input" role="search">
                <div class="form-group">
                    <input type="text" id="navbar-search-input" class="form-control" title="Enter address, city or ZIP code" placeholder="Enter address, city or ZIP code">
                </div>
                <a href="../search_view/search_view.php" class="btn btn-default"><span class="glyphicon glyphicon-search" aria-hidden="true"></span> Search</a>
            </form>
            <ul class="nav navbar-nav navbar-right">
                <li id="sign-in-button" ><a data-toggle="modal" data-target="#sign-in-modal" href="#">Sign in</a></li>
                <li id="register-button" ><a href="../sing_up/sing_up.php">Register</a></li>
                <li id="profile-toggle" class="active dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Profile <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li id="property-btn"><a href="../property_insert/property.php">Insert property</a></li>
                        <li class="active" id="dashboard-btn"><a href="../seller_dashboard/seller_dashboard.php">Dashboard</a></li>
                    </ul>
                </li>
                <li id="sign-out-button" ><a href="#">Sign out</a></li>
            </ul>
        </div>
    </div>
</nav>

<style>
    .navbar{
        background-color:#77bae4;
    }
    
    .navbar .nav > li > a {
        color:#222222;
    }
    
    .navbar-default .navbar-nav > .active >a{
        color: #FFF;
        background-color: rgba(0,0,70,0.5);
    }
    
    .navbar-nav>li:hover{
        color: #FFF !important;
        background-color:rgba(0,0,70,0.5);
    }
    
    .navbar-nav>li>a:hover{
        color: #FFF !important;
    }
    
    #profile-toggle[aria-expanded=true] {
        background-color: rgba(0,0,70,0.5);
        color:#ffffff;
    }
</style>
