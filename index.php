<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Homeblessed</title>
        <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="bower_components/fontawesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="bower_components/sweetalert/dist/sweetalert.css" rel="stylesheet">
        <link href="bower_components/sweetalert/dist/sweetalert.css" rel="stylesheet">
        <link href="assets/css/listings.css" rel="stylesheet">
        <link href="assets/css/index.css" rel="stylesheet">
        <link href="assets/css/navbar-index.css" rel="stylesheet">
        
        <link rel="icon" type="image/png" href="assets/img/homeblessed_favicon.png">
        
        <script type="application/javascript">
            var USER_SETTINGS=null;
            loadSettings();
            
            function loadSettings(){
                USER_SETTINGS=localStorage.getItem('USER_SETTINGS');
            }

            function saveSettings(settings){
                localStorage.setItem('USER_SETTINGS',settings);
            }
        </script>
        
    </head>
    <body>
        <nav class="navbar navbar-fixed-top navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="http://localhost/homeblessed/"><img id="homeblessed-logo-brand" src="assets/img/homeblessed_favicon.png"/></a>
                </div>

                <div class="collapse navbar-collapse">
                    <ul class="nav-main-options nav navbar-nav">
                        <li class="page-scroll active"><a href="#search-section">Home <span class="sr-only">(current)</span></a></li>
                        <li class="page-scroll"><a href="#why-homeblessed">Why us</a></li>
                        <li class="page-scroll"><a href="#new-listings">What's new</a></li>
                        <li class="page-scroll"><a href="#contact-us"> Contact us </a></li>
                    </ul>
                    <form class="navbar-form navbar-left" id="search-input" role="search">
                        <div class="form-group">
                            <input type="text" id="navbar-search-input" class="form-control" title="Enter address, city or ZIP code" placeholder="Enter address, city or ZIP code">
                        </div>
                        <button id="navbar-search-button-index" class="main-search btn btn-default"><span class="glyphicon glyphicon-search" aria-hidden="true"></span> Search</button>
                    </form>
                    <ul class="nav navbar-nav navbar-right">
                        <li id="sign-in-button" ><a data-toggle="modal" data-target="#sign-in-modal" href="#">Sign in</a></li>
                        <li id="register-button" ><a href="sing_up/sing_up.php">Register</a></li>
                        <li id="profile-toggle" class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Profile <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="property_insert/property.php">Insert property</a></li>
                                <li><a href="seller_dashboard/seller_dashboard.php">Dashboard</a></li>
                            </ul>
                        </li>
                        <li id="sign-out-button" ><a href="#">Sign out</a></li>
                    </ul>
                </div>
            </div>
        </nav>

        <section id="search-section">
            
            <a name="search-section-anchor"></a>
            
            <div class="centered jumbotron">
                <div class="container">
                    <h1>Find your home</h1>
                    <hr class="find-home-primary">
                    
                    <div class="row">
                        <div class="col-md-4"></div>
                        <a href="#"><div id="buy-btn" class="btn-selected col-md-2 col-xs search-btn"><h4>Buy</h4></div></a>
                        <a href="#"><div id="rent-btn" class="col-md-2 search-btn"><h4>Rent</h4></div></a>
                        <div class="col-md-4"></div>
                    </div>

                    <div class="row">
                        <div class="col-md-4"></div>
                        <div class="search-container col-md-4">
                            <div id="search-input2" class="input-group">
                                <input type="text" class="form-control" placeholder="Enter an address, city or ZIP code">
                                <span class="input-group-btn">
                                    <a href="search_view/search_view.php" class="main-search btn btn-default" type="button">Search</a>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-4"></div>
                    </div>
                </div>
            </div>
        </section>
        
        <section id="why-homeblessed">
            
            <a name="why-homeblessed-anchor"></a>
            
            <div class="centered container">
                <div class="row">
                    <h1>Why homeblessed?</h1>
                    <hr class="why-us-primary">
                </div>
                
                <div class="features row">
                    <div class="col-md-4">
                        <h3>We want what's best for you.</h3>
                        <img class="featurette-image img-responsive img-rounded center-block" data-src="" src="assets/img/house.jpg" alt="">
                        <p>We present to you the best estates, that are selected and evaluated by us, from all around the world.</p>
                    </div>
                    <div class="col-md-4">
                        <h3>We provide usefull statistics.</h3>
                        <img class="featurette-image img-responsive img-rounded center-block" data-src="" src="assets/img/statistics.jpg" alt="">
                        <p>Agents and owners can view their statistics in order to expand their sales and make a better strategy.</p>
                    </div>
                    <div class="col-md-4">
                        <h3>We care for your opinion.</h3>
                        <img class="featurette-image img-responsive img-rounded center-block" data-src="" src="assets/img/commute.jpg" alt="">
                        <p>Each user and possible buyer can give feedback on the service of an agent or owner in order for other users to see.</p>
                    </div>
                </div>
            </div>
        </section>
        
        <section id="new-listings">
            
            <a name="new-listings-anchor"></a>
            
            <div class="container">
               <div class="row">
                    <h1 class="centered">New listings</h1>
                    <hr class="listings-primary">
                </div>
                
                <div id="products" class="row list-group">
                    <div class="item col-md-4 ">
                        <div class="thumbnail">
                            <img class="group list-group-image" src="assets/img/listings/1.jpg" alt="" />
                            <div class="caption">
                                <h4 class="group inner list-group-item-heading">
                                    House<span class="text-muted"> Park Ave Shawnee 74804 </span></h4>
                                <p class="group inner list-group-item-text">
                                   Only minutes separate you from banking, shopping, colleges, medical facilities and much more in this cute home with a great oversized back yard! </p>
                                <div class="row">
                                    <div class="col-xs-12 col-md-6">
                                        <p class="lead">
                                            $55.000</p>
                                    </div>
                                    <div class="col-xs-12 col-md-6">
                                        <a class="btn btn-primary" href="estate_view/estate-view.php">View details</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item col-md-4 ">
                        <div class="thumbnail">
                            <img class="group list-group-image" src="assets/img/listings/2.jpg" alt="" />
                            <div class="caption">
                                <h4 class="group inner list-group-item-heading">
                                    House<span class="text-muted"> Park Ave Shawnee 74804 </span></h4>
                                <p class="group inner list-group-item-text">
                                   Only minutes separate you from banking, shopping, colleges, medical facilities and much more in this cute home with a great oversized back yard! </p>
                                <div class="row">
                                    <div class="col-xs-12 col-md-6">
                                        <p class="lead">
                                            $55.000</p>
                                    </div>
                                    <div class="col-xs-12 col-md-6">
                                        <a class="btn btn-primary" href="estate_view/estate-view.php">View details</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item  col-md-4 ">
                        <div class="thumbnail">
                            <img class="group list-group-image" src="assets/img/listings/5.jpg" alt="" />
                            <div class="caption">
                                <h4 class="group inner list-group-item-heading">
                                    Apartment<span class="text-muted"> Park Ave Shawnee 74803 </span></h4>
                                <p class="group inner list-group-item-text">
                                   Only minutes separate you from banking, shopping, colleges, medical facilities and much more in this cute home with a great oversized back yard! </p>
                                <div class="row">
                                    <div class="col-xs-12 col-md-6">
                                        <p class="lead">
                                            $450/month</p>
                                    </div>
                                    <div class="col-xs-12 col-md-6">
                                        <a class="btn btn-primary" href="estate_view/estate-view.php">View details</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item  col-md-4 ">
                        <div class="thumbnail">
                            <img class="group list-group-image" src="assets/img/listings/3.jpg" alt="" />
                            <div class="caption">
                                <h4 class="group inner list-group-item-heading">
                                    House<span class="text-muted"> Park Ave Shawnee 74804 </span></h4>
                                <p class="group inner list-group-item-text">
                                   Only minutes separate you from banking, shopping, colleges, medical facilities and much more in this cute home with a great oversized back yard! </p>
                                <div class="row">
                                    <div class="col-xs-12 col-md-6">
                                        <p class="lead">
                                            $55.000</p>
                                    </div>
                                    <div class="col-xs-12 col-md-6">
                                        <a class="btn btn-primary" href="estate_view/estate-view.php">View details</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item  col-md-4 ">
                        <div class="thumbnail">
                            <img class="group list-group-image" src="assets/img/listings/4.jpg" alt="" />
                            <div class="caption">
                                <h4 class="group inner list-group-item-heading">
                                    Apartment<span class="text-muted"> Park Ave Shawnee 74804 </span></h4>
                                <p class="group inner list-group-item-text">
                                   Only minutes separate you from banking, shopping, colleges, medical facilities and much more in this cute home with a great oversized back yard! </p>
                                <div class="row">
                                    <div class="col-xs-12 col-md-6">
                                        <p class="lead">
                                            $550/month</p>
                                    </div>
                                    <div class="col-xs-12 col-md-6">
                                        <a class="btn btn-primary" href="estate_view/estate-view.php">View details</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item  col-md-4 ">
                        <div class="thumbnail">
                            <img class="group list-group-image" src="assets/img/listings/6.jpg" alt="" />
                            <div class="caption">
                                <h4 class="group inner list-group-item-heading">
                                    Apartment<span class="text-muted"> Park Ave Shawnee 74804 </span></h4>
                                <p class="group inner list-group-item-text">
                                   Only minutes separate you from banking, shopping, colleges, medical facilities and much more in this cute home with a great oversized back yard! </p>
                                <div class="row">
                                    <div class="col-xs-12 col-md-6">
                                        <p class="lead">
                                            $250/month</p>
                                    </div>
                                    <div class="col-xs-12 col-md-6">
                                        <a class="btn btn-primary" href="estate_view/estate-view.php">View details</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
        
        <section id="contact-us">
            
            <a name="contact-us-anchor"></a>
            
            <div class="container">
                <div class="row section-header text-center">
                    <div class="col-md-12">
                        <h2>Contact Us</h2>
                        <hr class="contact-primary">
                    </div>
                </div>
                <div class="row contents">
                    <div class="col-md-12">
                        <form class="form-horizontal">
                            <fieldset>
                                <div class="form-group">  
                                    <input id="name-input" name="name-input" placeholder="Name" class="form-control" type="text">
                                </div>
                                <div class="form-group">
                                    <input id="email-input" name="email-input" placeholder="Email" class="form-control" type="text">
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" id="message-input" placeholder="Message" name="message-input"></textarea>
                                </div>
                                <div class="form-group">
                                    <button type="button" class="btn btn-default btn-lg"> Send </button>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </section>
                
        <footer class="footer">
            <div class="container">
                <span>Copyright © Homeblessed 2015 </span>
                <ul class="social-networks">
                    <li><a target="_blank" href="#"><i class="fa fa-facebook-square"></i></a></li>
                    <li><a target="_blank" href="#"><i class="fa fa-twitter-square"></i></a></li>
                    <li><a target="_blank" href="#"><i class="fa fa-instagram"></i></a></li>
                </ul>
            </div>
        </footer>
        
        <script src="bower_components/jquery/dist/jquery.min.js"></script>
        <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="bower_components/sweetalert/dist/sweetalert.min.js"></script>
        <script src="bower_components/bootstrap-validator/dist/validator.min.js"></script>
        <script src="bower_components/jquery.easing/js/jquery.easing.min.js"></script>
        <script src="assets/js/index.js"></script>
        <script src="assets/js/navbar.js"></script>
        <script src="assets/js/account.js"></script>
        
        <?php include('modals/sign-in.php'); ?>
    </body>
</html>